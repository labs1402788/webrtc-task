'use strict';
const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');

const sslkey = fs.readFileSync('ssl-key.pem');
const sslcert = fs.readFileSync('ssl-cert.pem')

const options = {
    key: sslkey,
    cert: sslcert
};

const app = express();

app.use(express.static("public"));

https.createServer(options, app).listen(3000);

app.get('/', (req, res) => {
    res.send('Hello Secure World!');
});